import Vue from 'vue'
import Vuex from 'vuex'
// import Localbase from 'localbase'

Vue.use(Vuex)

// let db = newLocalbase('db')
// db.config.debug = false

export default new Vuex.Store({
  state: {
    search: null,
    tasks: [],
    snackbar: {
      show: false,
      text: ''
    },
    sorting: false
  },
  mutations: {
    setSearch(state, value){
      state.search = value
    },

    addTask(state, newTask){
      state.tasks.push(newTask)
    },

    doneTask(state, id){
      let task = state.tasks.filter(task => task.id === id)[0]
      task.done = !task.done
    },

    deleteTask(state, id){
      state.tasks = state.tasks.filter(task => task.id !== id)
    },
    

  },
  actions: {
  },
  modules: {
  }
})
